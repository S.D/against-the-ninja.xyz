/* https://developer.mozilla.org/en-US/docs/Web/Media/Autoplay_guide
put some better error handling on playing and shit */

function handleFirstPlay(event) {
    /* if autoplay does go off, but the volume button in the right position */
    let player = event.target;
    console.log("here!");
    player.onplay = null;
    
    volume_element = document.getElementById("volume-control");
    volume_element.classList.remove("fa-volume-up");
    volume_element.classList.add("fa-volume-off");
}

function playPause() {
    player = document.getElementById("banging-tune");
    volume_element = document.getElementById("volume-control");
    if (volume_element.classList.contains("fa-volume-up"))
    {
        player.play();
        volume_element.classList.remove("fa-volume-up");
        volume_element.classList.add("fa-volume-off");                
    }
    else
    {
        player.pause();
        volume_element.classList.remove("fa-volume-off");
        volume_element.classList.add("fa-volume-up");                  
    }
}